import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.IntBinaryOperator;

public class Counter {
//    private int counter;
    AtomicInteger counter = new AtomicInteger(0);
//    private volatile int counter;

    public void setCounter(int newCounter) {
        counter.set(newCounter);
    }

    public int getCounter() {
        return counter.get();

//        return counter;
    }

    public int incrementCounter() {
        return counter.incrementAndGet();
//        return counter++;
    }

    //    public synchronized int incrementCounter() {
//       return counter++;
//    }

    public int decrementCounter() {
        return  counter.decrementAndGet();
    }

    public int getAndSetCounter (int newCounter){
        return counter.getAndSet(newCounter);
    }

    public int getAndAddCounter(int delta){
        return counter.getAndAdd(delta);
    }

    public boolean compareAndSetCounter(int expected, int update){
        return counter.compareAndSet(expected, update);
    }

    public int z(int value, IntBinaryOperator function) {
        return counter.accumulateAndGet(value, function);
    }


}
